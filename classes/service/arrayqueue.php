<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * キュー サービス
 */
class Service_ArrayQueue extends Service implements Interface_Service_Queue
{
  // コンテナでサービス名
  const _PROVIDES = 'service.queue';

  /**
   * キュー
   *
   * @access private
   */
  private static $queues = [];

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    \Config::load('wi-queue', true);
  }// </editor-fold>

  /**
   * キューを空にする
   *
   * @access public
   * @param string $queue キュー名
   * @return Interface_Service_Queue キュー サービス
   */
  public function flush($queue)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $filename = $this->get_filename($queue);
    file_put_contents($filename, json_encode([]));
    return $this;
  }// </editor-fold>

  /**
   * 一つ以上の要素をキューの最後に追加する
   *
   * @access public
   * @param string $queue キュー名
   * @param mixed $value 値
   * @return Interface_Service_Queue キュー サービス
   */
  public function push($queue, $value)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $filename = $this->get_filename($queue);
    $value = serialize($value);

    $array = json_decode(file_get_contents($filename), true);
    array_push($array, $value);
    file_put_contents($filename, json_encode($array));

    return $this;
  }// </editor-fold>

  /**
   * キューの末尾から要素を取り除く
   *
   * @access public
   * @param string $queue キュー名
   * @return mixed 項目が存在する場合は値、そうでない場合はnull
   */
  public function pop($queue)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $filename = $this->get_filename($queue);

    $array = json_decode(file_get_contents($filename), true);
    $value = array_pop($array);
    file_put_contents($filename, json_encode($array));

    $value = @unserialize($value);

    return $value;
  }// </editor-fold>

  /**
   * キューの先頭から要素を一つ取り出す
   *
   * @access public
   * @param string $queue キュー名
   * @return mixed 項目が存在する場合は値、そうでない場合はnull
   */
  public function shift($queue)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $filename = $this->get_filename($queue);

    $array = json_decode(file_get_contents($filename), true);
    $value = array_shift($array);
    file_put_contents($filename, json_encode($array));

    $value = @unserialize($value);

    return $value;
  }// </editor-fold>

  /**
   * 一つ以上の要素をキューの最初に加える
   *
   * @access public
   * @param string $queue キュー名
   * @param mixed $value 値
   * @return Interface_Service_Queue キュー サービス
   */
  public function unshift($queue, $value)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $filename = $this->get_filename($queue);
    $value = serialize($value);

    $array = json_decode(file_get_contents($filename), true);
    array_unshift($array, $value);
    file_put_contents($filename, json_encode($array));

    return $this;
  }// </editor-fold>

  /**
   * キューファイル名を取得する
   *
   * @access public
   * @param string $queue キュー名
   * @return string キューファイルのパス
   */
  protected function get_filename($queue)
  {
    $ds = DIRECTORY_SEPARATOR;
    $filename = rtrim(\Config::get('queue.path', $ds.'tmp'), $ds);
    $filename = $filename.$ds.'wi-queue.'.$queue;

    !file_exists($filename) and file_put_contents($filename, json_encode([]));

    return $filename;
  }
}
