<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * キューの設定
 */
return [
  /**
   * 配列キューのファイルのパス
   */
  'path' => '/tmp',
];
