<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * キュー サービス
 */
class Service_RedisQueue extends Service implements Interface_Service_Queue
{
  // コンテナでサービス名
  const _PROVIDES = 'service.queue';

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    \Redis_Db::forge('queue');
  }// </editor-fold>

  /**
   * キューを空にする
   *
   * @access public
   * @param string $queue キュー名
   * @return Interface_Service_Queue キュー サービス
   */
  public function flush($queue)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $queue = 'wi-queue.'.$queue;
    $redis = \Redis_Db::instance('queue');
    $redis->del($queue);
    return $this;
  }// </editor-fold>

  /**
   * 一つ以上の要素をキューの最後に追加する
   *
   * @access public
   * @param string $queue キュー名
   * @param mixed $value 値
   * @return Interface_Service_Queue キュー サービス
   */
  public function push($queue, $value)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $queue = 'wi-queue.'.$queue;
    $value = $this->serialize($value);
    $redis = \Redis_Db::instance('queue');
    $redis->rpush($queue, $value);
    return $this;
  }// </editor-fold>

  /**
   * キューの末尾から要素を取り除く
   *
   * @access public
   * @param string $queue キュー名
   * @return mixed 項目が存在する場合は値、そうでない場合はnull
   */
  public function pop($queue)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $queue = 'wi-queue.'.$queue;
    $redis = \Redis_Db::instance('queue');
    $value = $redis->rpop($queue);
    $value = $this->deserialize($value);
    return $value;
  }// </editor-fold>

  /**
   * キューの先頭から要素を一つ取り出す
   *
   * @access public
   * @param string $queue キュー名
   * @return mixed 項目が存在する場合は値、そうでない場合はnull
   */
  public function shift($queue)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $queue = 'wi-queue.'.$queue;
    $redis = \Redis_Db::instance('queue');
    $value = $redis->lpop($queue);
    $value = $this->deserialize($value);
    return $value;
  }// </editor-fold>

  /**
   * 一つ以上の要素をキューの最初に加える
   *
   * @access public
   * @param string $queue キュー名
   * @param mixed $value 値
   * @return Interface_Service_Queue キュー サービス
   */
  public function unshift($queue, $value)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $queue = 'wi-queue.'.$queue;
    $value = $this->serialize($value);
    $redis = \Redis_Db::instance('queue');
    $redis->lpush($queue, $value);
    return $this;
  }// </editor-fold>

  /**
   * シリアライズする
   *
   * @access protected
   * @param mixed 配列か文字列
   * @return string シリアライズされた文字列
   */
  protected function serialize($data)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (is_array($data))
    {
      foreach ($data as $key => $val)
      {
        if (is_string($val))
        {
          $data[$key] = str_replace('\\', '{{slash}}', $val);
        }
      }
    }
    else
    {
      if (is_string($data))
      {
        $data = str_replace('\\', '{{slash}}', $data);
      }
    }

    return serialize($data);
  }// </editor-fold>

  /**
   * デシリアライズする
   *
   * @access protected
   * @param string シリアライズされた文字列
   * @return mixed 配列か文字列
   */
  protected function deserialize($input)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $data = @unserialize($input);

    if (is_array($data))
    {
      foreach ($data as $key => $val)
      {
        if (is_string($val))
        {
          $data[$key] = str_replace('{{slash}}', '\\', $val);
        }
      }

      return $data;
    }

    elseif ($data === false)
    {
      is_string($input) and $data = array($input);
    }

    return (is_string($data)) ? str_replace('{{slash}}', '\\', $data) : $data;
  }// </editor-fold>
}
