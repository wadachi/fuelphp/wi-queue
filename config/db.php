<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * Redisデータベース設定
 */
return [
	'redis' => [
		'queue' => [
			'hostname' => '127.0.0.1',
			'port' => 6379,
			'database' => 0
		]
	]
];
