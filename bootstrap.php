<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Package::load('wi-container');

\Autoloader::add_classes([
  'Wi\\Interface_Service_Queue' => __DIR__.'/classes/interface/service/queue.php',
  'Wi\\Service_ArrayQueue' => __DIR__.'/classes/service/arrayqueue.php',
  'Wi\\Service_RedisQueue' => __DIR__.'/classes/service/redisqueue.php',
]);
