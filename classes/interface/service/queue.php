<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * キュー サービス
 */
interface Interface_Service_Queue
{
  /**
   * キューを空にする
   *
   * @access public
   * @param string $queue キュー名
   * @return Interface_Service_Queue キュー サービス
   */
  function flush($queue);

  /**
   * 一つ以上の要素をキューの最後に追加する
   *
   * @access public
   * @param string $queue キュー名
   * @param mixed $value 値
   * @return Interface_Service_Queue キュー サービス
   */
  function push($queue, $value);

  /**
   * キューの末尾から要素を取り除く
   *
   * @access public
   * @param string $queue キュー名
   * @return mixed 項目が存在する場合は値、そうでない場合はnull
   */
  function pop($queue);

  /**
   * キューの先頭から要素を一つ取り出す
   *
   * @access public
   * @param string $queue キュー名
   * @return mixed 項目が存在する場合は値、そうでない場合はnull
   */
  function shift($queue);

  /**
   * 一つ以上の要素をキューの最初に加える
   *
   * @access public
   * @param string $queue キュー名
   * @param mixed $value 値
   * @return Interface_Service_Queue キュー サービス
   */
  function unshift($queue, $value);
}
