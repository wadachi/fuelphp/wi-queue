<?php
/**
 * Wadachi FuelPHP Queue Package
 *
 * A queue service.
 *
 * @package    wi-queue
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 依存性注入コンテナの設定
 */
return [
  'service.queue' => [
    'class' => 'Wi\\Service_ArrayQueue',
  ],
];
